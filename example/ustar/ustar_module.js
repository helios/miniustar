 const app = getApp();
var QQMapWX = require('../../libs/qqmap-wx-jssdk.min.js');
var qqmapsdk;

module.exports = {
  data: {
    targetAddr: '杭州',
    latitude: 0,
    longitude: 0,
    remark: '',
    fee: 10,
    feenos: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50"],
    fees: ["1元", "2元", "3元", "4元", "5元", "6元", "7元", "8元", "9元", "10元", "11元", "12元", "13元", "14元", "15元", "16元", "17元", "18元", "19元", "20元", "21元", "22元", "23元", "24元", "25元", "26元", "27元", "28元", "29元", "30元", "31元", "32元", "33元", "34元", "35元", "36元", "37元", "38元", "39元", "40元", "41元", "42元", "43元", "44元", "45元", "46元", "47元", "48元", "49元", "50元"],
    feeIndex: 4,
    multiArray: [['全部问题'], ['全部问题']],
    multiIndex: [0, 0],
    multiId: [[0], [0]]
  },
  onLoad: function () {


    this.setData({
      targetAddr: '杭州',
      latitude: 0,
      longitude: 0,
      remark: '',
      fee: 10,
      feenos: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50"],
      fees: ["1元", "2元", "3元", "4元", "5元", "6元", "7元", "8元", "9元", "10元", "11元", "12元", "13元", "14元", "15元", "16元", "17元", "18元", "19元", "20元", "21元", "22元", "23元", "24元", "25元", "26元", "27元", "28元", "29元", "30元", "31元", "32元", "33元", "34元", "35元", "36元", "37元", "38元", "39元", "40元", "41元", "42元", "43元", "44元", "45元", "46元", "47元", "48元", "49元", "50元"],
      feeIndex: 4,
      multiArray: [['全部问题'], ['全部问题']],
      multiIndex: [0, 0],
      multiId: [[0], [0]]
    });

    wx.setNavigationBarTitle({
      title: '游大咖'
    });

    wx.setNavigationBarColor({
      frontColor: '#000000',
      backgroundColor: '#f8f8f8'
    });



    var _this = this;
    // 实例化API核心类
    qqmapsdk = new QQMapWX({
      key: 'QQPBZ-R573U-H2OVL-4F54Y-KACU5-OVFFT'
    });

    wx.getLocation({
      type: 'wgs84',
      success: function (res) {
        var latitude = res.latitude;
        var longitude = res.longitude;
        _this.setData({
          latitude: latitude,
          longitude: longitude
        });
        var speed = res.speed
        var accuracy = res.accuracy
        qqmapsdk.reverseGeocoder({
          location: {
            latitude: latitude,
            longitude: longitude
          },
          success: function (res) {
            console.log(res);
            _this.setData({
              targetAddr: res.result.address
            });
          },
          fail: function (res) {
            console.log(res);
          },
          complete: function (res) {
            console.log(res);
          }
        });
      }
    });


    var sql = "select cid,name from travel_class where isDelete=0 and level=1";
    wx.request({
      url: 'https://www.dpsoft.top/db.jsp', //仅为示例，并非真实的接口地址
      method: 'POST',
      data: {
        sql: sql
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: res => {
        console.log(res.data);
        if ("ok" == res.data.status) {
          var tags = new Array();
          var tagids = new Array();
          tags.push('全部问题');
          tagids.push(0);
          for (var i = 0; i < res.data.data.length; i++) {
            tags.push(res.data.data[i].name);
            tagids.push(res.data.data[i].cid);
          }
          var _multiArray = new Array();
          _multiArray.push(tags);
          tags = new Array();
          tags.push('全部问题');
          _multiArray.push(tags);
          /*
          tags = new Array();
          tags.push('全部问题');
          _multiArray.push(tags);
          */
          var _multiId = new Array();
          _multiId.push(tagids);
          tagids = new Array();
          tagids.push(0);
          _multiId.push(tagids);

          /*
          tagids = new Array();
          tagids.push(0);
          _multiId.push(tagids);
          */

          _this.setData({
            multiArray: _multiArray,
            multiId: _multiId
          });
        }
      }
    });
  },
  chooseTargetAddr: function (e) {
    var _this = this;
    wx.chooseLocation({
      success: function (res) {
        var name = res.name;
        console.log(name);
        _this.setData({
          targetAddr: name,
          latitude: res.latitude,
          longitude: res.longitude
        });
      },
      cancel: function () {

      },
      fail: function () {

      },
      complete: function () {

      }
    });
  },
  onCall: function (e) {

    var _this = this;
    wx.showLoading({
      title: "正在通知大咖..."
    });


    var sql = "select a.id from user_ask a ,`user` b where a.uid = b.uid and a.status < 5 and b.unionid='" + app.globalData.unionid + "'";
    wx.request({
      url: 'https://www.dpsoft.top/db.jsp', //仅为示例，并非真实的接口地址
      method: 'POST',
      data: {
        sql: sql
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: res => {
        console.log(res.data);
        if ("ok" == res.data.status) {

          if (res.data.data.length > 0) {
            wx.hideLoading();
            wx.showToast({
              title: '有未结束的订单',

              duration: 2000
            })
            return;
          }


          wx.request({
            url: 'https://www.dpsoft.top/indexmin.jsp', //仅为示例，并非真实的接口地址
            method: 'POST',
            data: {
              openid: app.globalData.unionid,
              Location_X: _this.data.longitude,
              Location_Y: _this.data.latitude,
              Label: _this.data.targetAddr,
              remark: _this.data.remark,
              price: _this.data.feenos[_this.data.feeIndex],
              classifyName: _this.data.multiArray[0][_this.data.multiIndex[0]] + "," + _this.data.multiArray[1][_this.data.multiIndex[1]],
              classifyId: _this.data.multiId[0][_this.data.multiIndex[0]] + "," + _this.data.multiId[1][_this.data.multiIndex[1]]
            },
            header: {
              'content-type': 'application/json' // 默认值
            },
            success: function (res) {
              console.log(res.data);

              wx.navigateBack();
            },
            complete: function (e) {
              wx.hideLoading();
            }
          });



        } else {
          wx.navigateBack();
          return;
        }
      }
    });






    //wx.navigateBack();

  },
  onFeeChange: function (e) {
    this.setData({
      fee: e.detail.value
    });

  },
  onFeeSelectChange: function (e) {

    this.setData({
      feeIndex: e.detail.value
    });
  },
  onRemark: function (e) {
    this.data.remark = e.detail.value;
  },
  bindMultiPickerChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      multiIndex: e.detail.value
    })
  },
  bindMultiPickerColumnChange: function (e) {
    console.log('修改的列为', e.detail.column, '，值为', e.detail.value);
    var data = {
      multiArray: this.data.multiArray,
      multiIndex: this.data.multiIndex
    };
    data.multiIndex[e.detail.column] = e.detail.value;
    switch (e.detail.column) {
      case 0:
        switch (data.multiIndex[0]) {
          case 0:
            data.multiArray[1] = ['全部问题'];
            //data.multiArray[2] = ['全部问题'];
            break;
          default:
            var sql = "select cid,name from travel_class where level = 2 and parentCid = " + this.data.multiId[0][data.multiIndex[0]];
            wx.request({
              url: 'https://www.dpsoft.top/db.jsp', //仅为示例，并非真实的接口地址
              method: 'POST',
              data: {
                sql: sql
              },
              header: {
                'content-type': 'application/json' // 默认值
              },
              success: res => {
                console.log(res.data);
                if ("ok" == res.data.status) {
                  var tags = new Array();
                  var tagids = new Array();
                  tags.push('全部问题');
                  tagids.push(0);
                  for (var i = 0; i < res.data.data.length; i++) {

                    tags.push(res.data.data[i].name);
                    tagids.push(res.data.data[i].cid);
                  }
                  data.multiArray[1] = tags;
                  this.data.multiId[1] = tagids;
                }
                //data.multiArray[2] = ['全部问题'];
                data.multiIndex[1] = 0;
                //data.multiIndex[2] = 0;
                console.log(data.multiArray);
                console.log(data.multiIndex);
                this.setData(data);
              }
            });



            break;
        }

        break;
      case 1:
        /*
          switch (data.multiIndex[1]) {
            case 0:

              data.multiArray[2] = ['全部问题'];
              break;
            default:
              var sql = "select cid,name from travel_class where level = 3 and parentCid = " + this.data.multiId[1][data.multiIndex[1]];
              wx.request({
                url: 'https://www.dpsoft.top/db.jsp', //仅为示例，并非真实的接口地址
                method: 'POST',
                data: {
                  sql: sql
                },
                header: {
                  'content-type': 'application/json' // 默认值
                },
                success: res => {
                  console.log(res.data);
                  if ("ok" == res.data.status) {
                    var tags = new Array();
                    var tagids = new Array();
                    tags.push('全部问题');
                    tagids.push(0);
                    for (var i = 0; i < res.data.data.length; i++) {

                      tags.push(res.data.data[i].name);
                      tagids.push(res.data.data[i].cid);
                    }
                    data.multiArray[2] = tags;
                    this.data.multiId[2] = tagids;
                  }

                  data.multiIndex[2] = 0;
                  console.log(data.multiArray);
                  console.log(data.multiIndex);
                  this.setData(data);

                }
              });



              break;
          }
*/


        break;
    }

  }
};