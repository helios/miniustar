const app = getApp();

Page({
    data: {
      userInfo:null
    },
    onLoad: function (options) {
      var _this = this;
      var sql = "select uid,tel,alipay_no,balance from user where unionid='" + app.globalData.unionid + "' ";
      if (options.uid != null){
        sql = "select uid,tel,alipay_no,balance from user where uid='" + options.uid + "' ";
      }
      console.info(sql);
      wx.request({
        url: 'https://www.dpsoft.top/db.jsp', //仅为示例，并非真实的接口地址
        method: 'POST',
        data: {
          sql: sql
        },
        header: {
          'content-type': 'application/json' // 默认值
        },
        success: res => {
          console.log(res.data);
          if ("ok" == res.data.status) {

            if (res.data.data.length > 0) {
              _this.setData({
                userInfo: res.data.data[0]
              });
            }
            


          } 
        }
      });
    },
    withdrawal: function (e) {
      var _this = this;
      wx.showLoading({
        title: "加载中..."
      });
      wx.request({
        url: 'https://www.dpsoft.top/transalipay.jsp', 
        method: 'POST',
        data: {
          openid: app.globalData.unionid,
          amount:_this.data.userInfo.balance,
          alipayaccount: _this.data.userInfo.alipay_no
        },
        header: {
          'content-type': 'application/json' // 默认值
        },
        success: res => {
          console.log(res.data);
          wx.hideLoading();
          if("ok" == res.data.status){
            console.log('提现成功');
            wx.showToast({
              title: '提现成功',
              duration: 2000
            });
            _this.onLoad({'uid':_this.data.userInfo.uid});

          }else{
            console.log(res.data.data);
            wx.showToast({
              title: res.data.data,
              duration: 2000
            })
          }
        },
        fail: e => {
          wx.hideLoading();
        },
        complete: e => {
          
        }
      });
    },
    setAlipayNo: function (e) {
      this.data.userInfo.alipay_no = e.detail.value;
    }

});