const app = getApp();
Page({
  data: {
    userInfo: null,
    askId: '',
    commentTxt:'',
    commentLevel:null,
    radioItems: [
      { name: '恶劣', value: '0' },
      { name: '较凶', value: '1', checked: true },
      { name: '正常', value: '2' },
      { name: '友好', value: '3' }
    ]
  },
  onLoad:function(options){
    var askId = options.askId;
    this.data.askId = askId;
    var _this = this;
    
    var sql = "select a.id, a.ask_no, a.remark, a.`status`, a.lng,a.lat,a.address, a.price, a.star_id, b.`name` nickName,b.head_img  avatarUrl , b.gender, c.uid staruid from user_ask a inner join `user` b on a.uid = b.uid inner join `user` c on a.star_id = c.uid and c.unionid='" + app.globalData.unionid + "' and a.status <3 ";

    if(askId != null){
      sql = "select a.id, a.ask_no, a.remark, a.`status`, a.lng,a.lat,a.address, a.price, a.star_id, b.`name` nickName,b.head_img  avatarUrl , b.gender, c.uid staruid from user_ask a inner join `user` b on a.uid = b.uid left JOIN `user` c on a.star_id = c.uid and c.unionid='" + app.globalData.unionid + "' where a.id=" + askId;
    }
    
    wx.request({
      url: 'https://www.dpsoft.top/db.jsp', //仅为示例，并非真实的接口地址
      method: 'POST',
      data: {
        sql: sql
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: res => {
        console.log(res.data);
        if("ok" == res.data.status){

          if (res.data.data.length <= 0) {
            wx.navigateBack();
            return;
          }
          _this.setData({
            userInfo : res.data.data[0]
          });


        }else{
          wx.navigateBack();
          return;
        }
      }
    });


       
  },
  onOrder: function(event){
    if(this.data.userInfo.status != 0){
      return;
    }
    var _this = this;

    wx.showLoading({
      title: "加载中..."
    });
    var sql = "select id from star_handle a,`user` b where a.star_uid = b.uid and a.status < 2 and b.unionid='" + app.globalData.unionid +"'";
    wx.request({
      url: 'https://www.dpsoft.top/db.jsp', //仅为示例，并非真实的接口地址
      method: 'POST',
      data: {
        sql: sql
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: res => {
        console.log(res.data);
        if ("ok" == res.data.status) {

          if (res.data.data.length > 0) {
            wx.hideLoading();
            wx.showToast({
              title: '当前的订单还没结束',
              duration: 2000
            })
            return;
          }
         

          wx.request({
            url: 'https://www.dpsoft.top/reply.jsp', //仅为示例，并非真实的接口地址
            method: 'POST',
            data: {
              openId: app.globalData.unionid,
              askId: _this.data.userInfo.id
            },
            header: {
              'content-type': 'application/json' // 默认值
            },
            success: function (res) {
              console.log(res.data);
              wx.navigateBack();
            },
            complete: function (e) {
              wx.hideLoading();
            }
          });




        } else {
          wx.navigateBack();
          return;
        }
      }
    });



    
    
  },
  unsettle : function(e){
    var _this = this;
    wx.showLoading({
      title: "加载中..."
    });
    wx.request({
      url: 'https://www.dpsoft.top/taskover.jsp', //仅为示例，并非真实的接口地址
      method: 'POST',
      data: {
        openId: app.globalData.unionid,
        askId: _this.data.userInfo.id,
        commentLevel: _this.data.commentLevel,
        commentTxt: _this.data.commentTxt,
        settled:0
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        console.log(res.data);
        wx.navigateBack();
      },
      complete: function (e) {
        wx.hideLoading();
      }
    })
  },
  settled:function(e){
    var _this = this;
    wx.showLoading({
      title: "加载中..."
    });
    wx.request({
      url: 'https://www.dpsoft.top/taskover.jsp', //仅为示例，并非真实的接口地址
      method: 'POST',
      data: {
        openId: app.globalData.unionid,
        askId: _this.data.userInfo.id,
        commentLevel: _this.data.commentLevel,
        commentTxt: _this.data.commentTxt,
        settled: 1
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        console.log(res.data);
        wx.navigateBack();
      },
      complete: function (e) {
        wx.hideLoading();
      }
    })
  },
  oncomment:function(e){
    this.data.commentTxt = e.detail.value;
  },
  oncommentLevel:function(e){
    this.data.commentLevel = e.detail.value;
  },
  openmap: function (e) {
    wx.navigateTo({ url: "/example/map/map?latitude=" + this.data.userInfo.lat + "&longitude=" + this.data.userInfo.lng });
  }
});