const app = getApp();
Page({
  data: {
    askInfo: null,
    commentTxt: '',
    commentLevel: null,
    radioItems: [
      { name: '很差', value: '0' },
      { name: '不行', value: '1', checked: true },
      { name: '可以', value: '2' },
      { name: '专业', value: '3' }
    ]
  },
  onLoad:function(options){
    var askId = options.askId;
    var _this = this;

    var sql = "select a.id, a.ask_no, a.remark, a.`status`,a.lng,a.lat, a.address, a.price, a.star_id,a.settled, b.`name`, b.head_img, b.gender, b.identity, b.occupation ,,b.openid staropenid from user_ask a inner join  `user` c on a.uid=c.uid left JOIN `user` b on a.star_id = b.uid where a.status=0 and c.unionid='" + app.globalData.unionid+"'";

    if(askId != null){
      sql ="select a.id, a.ask_no, a.remark, a.`status`,a.lng,a.lat, a.address, a.price, a.star_id,a.settled, b.`name`, b.head_img, b.gender, b.identity, b.occupation,b.openid staropenid from user_ask a left JOIN `user` b on a.star_id = b.uid where a.id="+askId;
    }
    wx.request({
      url: 'https://www.dpsoft.top/db.jsp', //仅为示例，并非真实的接口地址
      method: 'POST',
      data: {
        sql: sql
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: res => {
        console.log(res.data);
        if("ok" == res.data.status){
          if (res.data.data.length <= 0){
            wx.navigateBack();
            return;
            }
          _this.setData({
            askInfo : res.data.data[0]
          });


        }else{
          wx.navigateBack();
          return;
        }
      }
    });


       
  },
  exitcall: function(event){
   
    wx.showLoading({
      title: "加载中..."
    });
    var _this = this;
    wx.request({
      url: 'https://www.dpsoft.top/exitcall.jsp', //仅为示例，并非真实的接口地址
      method: 'POST',
      data: {
        askId: _this.data.askInfo.id,
        openId: app.globalData.unionid
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        console.log(res.data);
        wx.navigateBack();
      },
      complete: function (e) {
        wx.hideLoading();
      }
    })
  },
  appeal: function (event) {
  
    wx.showLoading({
      title: "加载中..."
    });
    var _this = this;
    wx.request({
      url: 'https://www.dpsoft.top/callend.jsp', //仅为示例，并非真实的接口地址
      method: 'POST',
      data: {
        askId: _this.data.askInfo.id,
        commentLevel: _this.data.commentLevel,
        commentTxt: _this.data.commentTxt,
        openId: app.globalData.unionid,
        endstatus: 5,
        starOpenId: _this.data.askInfo.staropenid,
        starId: _this.data.askInfo.star_id,
        price: _this.data.askInfo.price,
        starName: _this.data.askInfo.name
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        console.log(res.data);
        wx.navigateBack();
      },
      complete: function (e) {
        wx.hideLoading();
      }
    })
  },
  pay: function (event) {


    wx.showLoading({
      title: "加载中..."
    });
    var _this = this;
    wx.request({
      url: 'https://www.dpsoft.top/pay.jsp', //仅为示例，并非真实的接口地址
      method: 'POST',
      data: {
        askId: _this.data.askInfo.id,
        openId: app.globalData.openId,
        staropenid: _this.data.askInfo.staropenid,
        fee: _this.data.askInfo.price*100
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        console.log(res.data);
        if (res.data.paySign!=null){
          var reqdata = res.data;
          reqdata.success = function(e){
            wx.showLoading({
              title: "支付成功..."
            });
            wx.request({
              url: 'https://www.dpsoft.top/callend.jsp', //仅为示例，并非真实的接口地址
              method: 'POST',
              data: {
                askId: _this.data.askInfo.id,
                commentLevel: _this.data.commentLevel,
                commentTxt: _this.data.commentTxt,
                openId: app.globalData.unionid,
                endstatus: 6,
                starOpenId: _this.data.askInfo.staropenid,
                starId: _this.data.askInfo.star_id,
                price: _this.data.askInfo.price,
                starName: _this.data.askInfo.name
              },
              header: {
                'content-type': 'application/json' // 默认值
              },
              success: function (res) {
                console.log(res.data);
                wx.navigateBack();
              },
              complete: function (e) {
                wx.hideLoading();
              }
            })

          };
          wx.requestPayment(reqdata);
        }
       

       
      },
      complete: function (e) {
        wx.hideLoading();
      }
    })



    



  },
  nopay: function (event) {
    
    wx.showLoading({
      title: "加载中..."
    });
    var _this = this;
    wx.request({
      url: 'https://www.dpsoft.top/callend.jsp', //仅为示例，并非真实的接口地址
      method: 'POST',
      data: {
        askId: _this.data.askInfo.id,
        commentLevel: _this.data.commentLevel,
        commentTxt: _this.data.commentTxt,
        openId: app.globalData.unionid,
        endstatus: 5,
        starOpenId: _this.data.askInfo.staropenid,
        starId: _this.data.askInfo.star_id,
        price: _this.data.askInfo.price,
        starName: _this.data.askInfo.name
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        console.log(res.data);
        wx.navigateBack();
      },
      complete: function (e) {
        wx.hideLoading();
      }
    })
  },
  oncomment: function (e) {
    this.data.commentTxt = e.detail.value;
  },
  oncommentLevel: function (e) {
    this.data.commentLevel = e.detail.value;
  },
  openmap: function (e) {
    wx.navigateTo({url: "/example/map/map?latitude=" + this.data.askInfo.lat + "&longitude=" + this.data.askInfo.lng});
  }
});