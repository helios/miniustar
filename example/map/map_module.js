module.exports = {
  data: {

    scale: 14,
    latitude: 30.2229188879,
    longitude: 120.1268935204,
    markers: [{

      id: 0,
      latitude: 30.2229188879,
      longitude: 120.1268935204
    }],

  },
  onLoad: function (options) {

    wx.setNavigationBarTitle({
      title: '地理位置'
    });

    wx.setNavigationBarColor({
      frontColor: '#000000',
      backgroundColor: '#f8f8f8'
    });


    this.setData({

      scale: 14,
      latitude: 30.2229188879,
      longitude: 120.1268935204,
      markers: [{

        id: 0,
        latitude: 30.2229188879,
        longitude: 120.1268935204
      }],

    });
  },
  regionchange(e) {
    console.log(e.type)
  },
  markertap(e) {
    console.log(e.markerId)
  },
  controltap(e) {
    console.log(e.controlId)
  }
}