Page({
  data: {
   
    scale:14,
      latitude: 23.099994,
      longitude: 113.324520,
   markers: [{
    
        id: 0,
        latitude: 23.099994,
        longitude: 113.324520
      }],
   
  },
  onLoad: function (options) {
    this.setData({
      latitude:options.latitude,
      longitude:options.longitude,
      markers:[{
        latitude: options.latitude,
        longitude: options.longitude
      }]
    });
  },
  regionchange(e) {
    console.log(e.type)
  },
  markertap(e) {
    console.log(e.markerId)
  },
  controltap(e) {
    console.log(e.controlId)
  }
})