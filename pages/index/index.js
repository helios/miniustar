// pages/index/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    showpage:'circle'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var showpage = 'map';
    
    if (typeof (options) != 'undefined' && null != options.page){
      showpage = options.page;
    }
    if ('ustar' != showpage && 'map' != showpage){
      showpage = options.page;
      var modulejs = require("../" + showpage + "/" + showpage + "_module.js");

      for (var p in modulejs) {
        this[p] = modulejs[p];
      }
    }else{
      var modulejs = require("../../example/" + showpage + "/" + showpage + "_module.js");

      for (var p in modulejs) {
        this[p] = modulejs[p];
      }
    }
    console.log(showpage);
   
    
   
    this.onLoad(options);
    this.setData({
      showpage: showpage
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})