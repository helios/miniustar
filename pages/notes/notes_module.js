var app = getApp();

module.exports = {

  /**
   * 页面的初始数据
   */
  data: {
    ticket:null,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var _this = this;
    this.setData({
      ticket: null,
      notes: null
      });
    var id = options.id;
    var spotname = options.spotname;

    var sql = "select * from ticket where data_id='"+id+"'";
    app.exesql(sql,function(res){
        var ticket = res.data.data[0];
        ticket.spotname = spotname;
        var notes = JSON.parse(ticket.ticket_desc);
        for (var x in notes.data){
          for (var y in notes.data[x].subDetailList){
            notes.data[x].subDetailList[y].desc = notes.data[x].subDetailList[y].desc.replace(/<br\/>/g,"\r\n").replace(/携程/g,"游大咖");
          console.log(notes.data[x].subDetailList[y].desc);
          }
        }
        _this.setData({
          ticket:ticket,
          notes:notes
        });
    });
  },
  onconfirm:function(){
    
    wx.navigateTo({
      url: '/pages/index/index?page=ticketpay&ticketspot=' + this.data.ticket.spot_code + '&price=' + this.data.ticket.my_price + '&ticketname=' + this.data.ticket.ticket_name + '&preordertime=' + this.data.ticket.pre_order_time + '&dataid=' + this.data.ticket.data_id+'&spotname='+this.data.ticket.spotname,
    })
  }
}