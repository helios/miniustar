var app = getApp()

module.exports = {

  /**
   * 页面的初始数据
   */
  data: {
    imglist:[],
    spot:null,
    ticketlist:[],
    ticketmap:null,
    typelist:[],
    location:null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      imglist: [],
      spot: null,
      ticketlist: [],
      ticketmap: null,
      typelist: [],
      location: null
      });






      var _this = this;
      var spotCode = options.spotCode;

      var sql = "select * from ticket_spot where spot_code='"+spotCode+"'";
      app.exesql(sql,function(res){
          var spot = res.data.data[0];
          var imgjson = spot.img_json;
          var imglist = JSON.parse(imgjson);
          _this.setData({
            imglist:imglist,
            spot:spot
          });

         


      },function(res){});

      sql = "select * from ticket where spot_code='" + spotCode + "'";
      app.exesql(sql, function (res) {
        var ticketlist = res.data.data;
        var ticketmap = new Map();
        for(var x in ticketlist){
          var ticket = ticketlist[x];
          var thisticketlist = ticketmap.get(ticket.ticket_type);
          if(null == thisticketlist){
            thisticketlist = new Array();
          }
          thisticketlist.push(ticket);
          ticketmap.set(ticket.ticket_type, thisticketlist);
        }

        console.log(ticketmap);
        ticketmap.forEach(function(value,key,thismap){
          var tlist = value;
          var minprice = 9999;
          for(var x in tlist){
            var price = tlist[x].my_price;
            
            if (Number(price.substring(1)) < minprice){
              minprice = Number(price.substring(1));
            }
          }
          _this.data.typelist.push({id:key,type:key,minprice:minprice,list:value,open:false});
        });
        _this.setData({
          ticketlist: ticketlist,
          typelist: _this.data.typelist,
          ticketmap: ticketmap
        });

      }, function (res) { });
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  kindToggle: function(e){

    var id = e.currentTarget.id;
    for(var x in this.data.typelist){
      
      if (id == this.data.typelist[x].id){
        this.data.typelist[x].open = !this.data.typelist[x].open;
      }else{
        this.data.typelist[x].open = false;
      }
    }
    this.setData({
      typelist: this.data.typelist
    });
  },
  openDesc: function(){
    var spotCode = this.data.spot.spot_code;
    var sql = "select spot_desc from ticket_spot where spot_code='" + spotCode + "'";
    var url = "https://www.dpsoft.top/dbhtml.jsp?sql=" + encodeURIComponent(sql);
    console.log(url);
   wx.navigateTo({
     url: '/pages/dbhtml/dbhtml?url=' +encodeURIComponent(url),
   })
  },
  openTrafic: function(){
    var spotCode = this.data.spot.spot_code;
    var sql = "select trafic from ticket_spot where spot_code='" + spotCode + "'";
    var url = "https://www.dpsoft.top/dbhtml.jsp?sql=" + encodeURIComponent(sql);
    console.log(url);
    wx.navigateTo({
      url: '/pages/dbhtml/dbhtml?url=' + encodeURIComponent(url),
    })
  },
  openmap:function(){
    wx.openLocation({
      latitude: this.data.spot.lat,
      longitude: this.data.spot.lng,
      scale: 28
    })
  }
}