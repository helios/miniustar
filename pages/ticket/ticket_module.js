
var app = getApp()
var QQMapWX = require('../../libs/qqmap-wx-jssdk.min.js');
var qqmapsdk;
module.exports = {

  data: {
    inputShowed: false,
    inputVal: "",
    ticketlist:[]
  },
  onLoad: function (options) {
    var _this = this;
    this.setData({
      inputShowed: false,
      inputVal: "",
      ticketlist: []
    });

    // 实例化API核心类
    qqmapsdk = new QQMapWX({
      key: 'QQPBZ-R573U-H2OVL-4F54Y-KACU5-OVFFT'
    });

    var sql = "select code,name from ticket_address where level = 1";

    app.exesql(sql,function(res){
      var provincelist = res.data.data;
      var provincenamelist = new Array();
      var provincecodelist = new Array();
      for(var x in provincelist){
        provincenamelist.push(provincelist[x].name);
        provincecodelist.push(provincelist[x].code);
      }
      var addressnamearray = new Array();
      addressnamearray.push(provincenamelist);
      addressnamearray.push(['全部']);

      var addresscodearray = new Array();
      addresscodearray.push(provincecodelist);
      addresscodearray.push(['全部']);

      _this.setData({
        addressnamearray: addressnamearray,
        addresscodearray: addresscodearray,
        addressindex: [0,0]
      });
   

    wx.getLocation({
      type: 'wgs84',
      success: function (res) {
        var latitude = res.latitude;
        var longitude = res.longitude;

        //var latitude = 30.1739512149;
        //var longitude = 120.1021258303;


        _this.setData({
          location: { latitude: latitude, longitude: longitude}
        });
        
        qqmapsdk.reverseGeocoder({
          location: {
            latitude: latitude,
            longitude: longitude
          },
          success: function (res) {
            console.log(res);
            var province = res.result.address_component.province;
            var city = res.result.address_component.city;
            

            for (var x in _this.data.addressnamearray[0]){
              if (province == _this.data.addressnamearray[0][x]){
                _this.data.addressindex[0] = x;
              }
            }


            var sql = "select name, code from ticket_address where parent_code='" + _this.data.addresscodearray[0][_this.data.addressindex[0]] + "' ";

            app.exesql(sql, function (res) {
              var citylist = res.data.data;
              var citynamelist = new Array();
              var citycodelist = new Array();
              for (var x in citylist) {
                citynamelist.push(citylist[x].name);
                citycodelist.push(citylist[x].code);
                if (citylist[x].name == city){
                  _this.data.addressindex[1] = x;
                }
              }
              var addressnamearray = _this.data.addressnamearray;
              addressnamearray[1] = citynamelist;

              var addresscodearray = _this.data.addresscodearray;
              addresscodearray[1] = citycodelist;
              _this.setData({
                addressnamearray: addressnamearray,
                addresscodearray: addresscodearray,
                addressindex: _this.data.addressindex,
                currprovince:province,
                currcity:city
              });


              _this.getData();
            });


          },
          fail: function (res) {
            console.log(res);
          },
          complete: function (res) {
            console.log(res);
          }
        });

      },
      fail: function (res) {
        
        console.log("获取位置失败");

        var sql = "select name, code from ticket_address where parent_code='" + _this.data.addresscodearray[0][_this.data.addressindex[0]]+"'";

        app.exesql(sql, function(res){
          var citylist = res.data.data;
          var citynamelist = new Array();
          var citycodelist = new Array();
          for (var x in citylist) {
            citynamelist.push(citylist[x].name);
            citycodelist.push(citylist[x].code);
          }
          var addressnamearray = _this.data.addressnamearray;
          addressnamearray[1] = citynamelist;

          var addresscodearray = _this.data.addresscodearray;
          addresscodearray[1] = citycodelist;
          _this.setData({
            addressnamearray: addressnamearray,
            addresscodearray: addresscodearray
          });


          _this.getData();

        });

      }
    });
    });

    




  },
  onPullDownRefresh: function () {
    wx.showNavigationBarLoading()

    console.log("upper");



    wx.showLoading({
      title: "刷新中"
    });

    this.getData(function () {
      wx.hideLoading();

      wx.hideNavigationBarLoading();
    },0);

    
  },
  onReachBottom: function (e) {
    wx.showNavigationBarLoading();
    var that = this;
  
   this.getData(function () {
        wx.hideLoading();

        wx.hideNavigationBarLoading();
      }, 1);
      
    console.log("lower")
  },
  getData: function (cb,next) {
    var _this = this;
    var sql = "select t.spot_name,t.grade,t.jgrade,t.classifylist,t.feature,t.spot_addr,t.price,t.market_price,t.spot_img,t.spot_code from ticket_spot t where t.price is not null ";

    if (_this.data.keyword != null && _this.data.keyword.trim().length>0){
      sql = sql + " and spot_name like '%" + _this.data.keyword +"%' ";
}else{



    sql = sql + " and province_code='" + _this.data.addresscodearray[0][_this.data.addressindex[0]] + "' and city_code='" + _this.data.addresscodearray[1][_this.data.addressindex[1]] + "' "
    }
    if (_this.data.location != null && _this.data.currprovince == _this.data.addressnamearray[0][_this.data.addressindex[0]] && this.data.currcity == _this.data.addressnamearray[1][_this.data.addressindex[1]]){
      sql = sql + " order by (lat-" + _this.data.location.latitude + ")*(lat-" + _this.data.location.latitude + ") + (lng-" + _this.data.location.longitude + ")*(lng-" + _this.data.location.longitude + ")";
    }
   
   if(next == 1){
     sql = sql + " limit " + this.data.ticketlist.length +", 10";
   }else{
     sql = sql + " limit 10";
   }

    console.log(sql);
    wx.request({
      url: 'https://www.dpsoft.top/db.jsp', //仅为示例，并非真实的接口地址
      method: 'POST',
      data: {
        sql: sql
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: res => {
        console.log(res.data);

        if ("ok" == res.data.status) {
          var feed_data = res.data.data;
          for (var x in feed_data) {
            feed_data[x].classes = feed_data[x].classifylist.split(",");

          }
          if(next == 1){
            feed_data = _this.data.ticketlist.concat(feed_data);
          }
          _this.setData({
            ticketlist: feed_data
           
          });

         
          typeof cb == "function" && cb();


        } else {

        }
      }
    });
  },
  showInput: function () {
    this.setData({
      inputShowed: true
    });
  },
  hideInput: function () {
    this.setData({
      inputVal: "",
      inputShowed: false,
      keyword: null
    });
    
    this.getData();
  },
  clearInput: function () {
    this.setData({
      inputVal: ""
    });
  },
  inputTyping: function (e) {
    this.setData({
      inputVal: e.detail.value
    });
  },
  bindMultiPickerChange: function(e){
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      addressindex: e.detail.value
    })

    

  },
  bindMultiPickerColumnChange:function(e){
    var _this =this;
    console.log('修改的列为', e.detail.column, '，值为', e.detail.value);
    var data = {
      addressnamearray: this.data.addressnamearray,
      addressindex: this.data.addressindex
    };
    data.addressindex[e.detail.column] = e.detail.value;
    switch (e.detail.column) {
      case 0:
        
        var sql = "select name, code from ticket_address where parent_code='" + _this.data.addresscodearray[0][e.detail.value] + "'";

        app.exesql(sql, function (res) {
          var citylist = res.data.data;
          var citynamelist = new Array();
          var citycodelist = new Array();
          for (var x in citylist) {
            citynamelist.push(citylist[x].name);
            citycodelist.push(citylist[x].code);
          }
          var addressnamearray = _this.data.addressnamearray;
          addressnamearray[1] = citynamelist;

          var addresscodearray = _this.data.addresscodearray;
          addresscodearray[1] = citycodelist;
          _this.data.addressindex[1] = 0;
          _this.setData({
            addressnamearray: addressnamearray,
            addresscodearray: addresscodearray,
            addressindex: _this.data.addressindex
          });


          _this.getData();

        });

        break;

      case 1:
        _this.setData({
          addressindex: data.addressindex
        });
        _this.getData();
        break;
    }
  },
  inputTyping: function(e){
    this.setData({
      keyword: e.detail.value
    });
  },
  search:function(e){
    this.getData();
  }

}