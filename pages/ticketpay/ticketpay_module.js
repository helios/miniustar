var app = getApp();

module.exports = {

  /**
   * 页面的初始数据
   */
  data: {
    buycount: ["1张", "2张", "3张", "4张", "5张", "6张", "7张", "8张", "9张", "10张"],
    buycountIndex: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var spotname = options.spotname;
    var spotcode = options.ticketspot;
    var price = options.price.substring(1);
    var dataid = options.dataid;
    var preordertime = options.preordertime;
    var ticketname = options.ticketname;

    var reg = new RegExp("当天(\\d+):(\\d+)前", "g");
    var days = 0;
    var hours = 23;
    var minutes = 59;
    var result = null;
    if ((result = reg.exec(preordertime))!=null){
        hours = result[1];
        minutes = result[2];
    }

    reg = new RegExp("游玩前(\\d+)天(\\d+):(\\d+)前", "g");
    if ((result = reg.exec(preordertime)) != null) {
      days = Number(result[1]);
      hours = Number(result[2]);
      minutes = Number(result[3]);
    }

    var startdate = new Date();
    
    if (days < 1 && (startdate.getHours() > hours || (startdate.getHours() == hours && startdate.getMinutes() >= minutes ))){
      days = 1;
    }
    console.log(startdate.getDate());
    startdate.setDate(startdate.getDate() + days);
    console.log(startdate.getDate());
    var enddate = new Date();
    enddate.setMonth(enddate.getMonth()+1);

    var usedate = startdate;

    this.setData({
      spotname:spotname,
      spotcode:spotcode,
      price:price,
      dataid:dataid,
      preordertime:preordertime,
      ticketname:ticketname,
      buycount: ["1张", "2张", "3张", "4张", "5张", "6张", "7张", "8张", "9张", "10张"],
      buycountIndex: 0,
      startdate: this.formatdate(startdate),
      enddate: this.formatdate(enddate),
      usedate: this.formatdate(usedate),
      payamount:price
    });
    console.log(this.data);
  },
  selectcount: function(e){
    this.setData({
      buycountIndex: e.detail.value,
      payamount: Number(this.data.price) * (Number(e.detail.value)+1)
    })
  },
  bindDateChange: function(e){
    this.setData({
      usedate: e.detail.value
    });
  },
  pay: function (event) {

    if(this.data.contactname == null){
      wx.showModal({
        title: '输入提示',
        content: '请输入联系人姓名',
        showCancel: false,
        success: function (res) {
          
        }
      });
      return;
    }

    if (this.data.contacttel == null) {
      wx.showModal({
        title: '输入提示',
        content: '请输入联系手机号',
        showCancel: false,
        success: function (res) {

        }
      });
      return;
    }

    if (!new RegExp("^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\\d{8}$").test(this.data.contacttel)){
      wx.showModal({
        title: '输入提示',
        content: '请输入正确的手机号',
        showCancel: false,
        success: function (res) {

        }
      });
      return;
    }


    var regIdNo = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
    if (this.data.contacttel == null || !regIdNo.test(this.data.contactid)) {
      wx.showModal({
        title: '输入提示',
        content: '请输入正确的身份证号',
        showCancel: false,
        success: function (res) {

        }
      });
      return;
    }

    wx.showLoading({
      title: "加载中..."
    });
    var _this = this;

    var orderseqno = this.guid();// this.data.dataid + app.globalData.unionid + new Date().getTime();
    var sql = "insert into ticket_order (spot_name,spot_code,ticket_name,data_id,use_date,buy_count,amount,price,ordertime,contact_name,contact_tel,status,security_code,qrcode,unionid,seqno,contact_id) values('" + _this.data.spotname + "', '" + _this.data.spotcode + "','" + _this.data.ticketname + "','" + _this.data.dataid + "','" + _this.data.usedate + "'," + (_this.data.buycountIndex + 1) + "," + _this.data.payamount + "," + _this.data.price + "," + new Date().getTime() + ",'" + this.data.contactname + "','" + this.data.contacttel + "',0,'','','" + app.globalData.unionid + "','" + orderseqno + "','" + this.data.contactid +"')";
    app.exesql(sql, function(res){
       


      wx.request({
        url: 'https://www.dpsoft.top/compay.jsp', //仅为示例，并非真实的接口地址
        method: 'POST',
        data: {
          tradeno: orderseqno,
          openId: app.globalData.openId,
          amount: _this.data.payamount * 100,
          body: "门票-" + _this.data.ticketname
        },
        header: {
          'content-type': 'application/json' // 默认值
        },
        success: function (res) {
          console.log(res.data);
          if (res.data.paySign != null) {
            var reqdata = res.data;
            reqdata.success = function (e) {
              wx.showLoading({
                title: "支付成功"
              });

              var sql = "update ticket_order set status = 1 where seqno = '" + orderseqno+"'";
              app.exesql(sql,function(res){
                  wx.navigateTo({
                    url: '/pages/index/index?page=ticketorder',
                  });
                  wx.hideLoading();
              });


            };
            wx.requestPayment(reqdata);
          }
        },
        complete: function (e) {
          wx.hideLoading();
        }
      })


    },function(){
      wx.hideLoading();
    });

  },
  setContactName:function(e){
    this.setData({
      contactname: e.detail.value
    });
  },
  setContactTel:function(e){
    this.setData({
      contacttel: e.detail.value
    });
  },
  setContactID: function(e){
    this.setData({
      contactid: e.detail.value
    });
  },
  formatdate: function(date) {

    var d = date.getDate();
    var m = date.getMonth() + 1;
    var y = date.getFullYear();
    if (m < 10) {
      m = '0' + m;
    };
    if (d < 10) {
      d = '0' + d;
    };
    return y + "-" + m + "-" + d;
  },
  guid: function() {
  return 'xyxyxyxyxyxyxyxy'.replace(/[xy]/g, function (c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}
}