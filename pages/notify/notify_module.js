//logs.js
var app = getApp()
module.exports = {
  data: {
    usermsg: []
  },
  onLoad: function () {
    this.setData({
      usermsg: []
    });
    wx.setNavigationBarTitle({
      title: '消息列表'
    })
    var _this = this;
    var sql = "select a.nick_name,a.head_img,b.msg_type,b.dynamic_id,b.msg_unionid,b.status,b.createtime,b.msg from user a,user_msg b where a.unionid=b.msg_unionid and b.unionid='" + app.globalData.unionid + "' and b.status = 0";
    app.exesql(sql, function (res) {
      for (var x in res.data.data) {
        res.data.data[x].msg = decodeURI(res.data.data[x].msg);
      }
      _this.setData({
        usermsg: res.data.data
      });
      var sql = "update user_msg set status=1 where unionid='" + app.globalData.unionid + "' and status = 0";
      app.exesql(sql);

    }, function (res) {
      wx.showToast({
        title: '失败，请重试',
        icon: 'fail',
        duration: 2000
      });
    });


  },
  gotoDetail: function (e) {
    var dynamic_id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/index/index?page=detail&dynamic_id=' + dynamic_id,
    })
  }
};
