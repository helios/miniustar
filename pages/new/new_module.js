// pages/new/new.js
const app = getApp();
//var QQMapWX = require('../../libs/qqmap-wx-jssdk.min.js');
//var qqmapsdk;
module.exports = {

  /**
   * 页面的初始数据
   */
  data: {
    newtxt: '',
    newimgs: [],
    imgWebPath: [],
    location: { 'name': '', 'lat': 0, 'lng': 0 },
    animationData: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      newtxt: '',
      newimgs: [],
      imgWebPath: [],
      location: { 'name': '', 'lat': 0, 'lng': 0 },
      animationData: {}
    });
    wx.setNavigationBarTitle({
      title: '发表动态'
    })
    /*
      // 实例化API核心类
      qqmapsdk = new QQMapWX({
        key: 'QQPBZ-R573U-H2OVL-4F54Y-KACU5-OVFFT'
      });
  
      wx.getLocation({
        type: 'wgs84',
        success: function (res) {
          var latitude = res.latitude;
          var longitude = res.longitude;
         
          var speed = res.speed
          var accuracy = res.accuracy
          qqmapsdk.reverseGeocoder({
            location: {
              latitude: latitude,
              longitude: longitude
            },
            success: function (res) {
              console.log(res);
              _this.setData({
                location: {
                  name: res.result.address, lat: latitude, lng: longitude}
              });
            },
            fail: function (res) {
              console.log(res);
            },
            complete: function (res) {
              console.log(res);
            }
          });
        }
      });
      */

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    /*
    wx.hideTabBar({ animation:true});
    var animation = wx.createAnimation({ duration: 1000, timingFunction: 'ease' });
    animation.translateY(-1000).step();

    this.setData({
      animationData: animation.export()
    });
    */

  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {


  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  onAddImg: function () {
    var _this = this;
    wx.chooseImage({
      count: 9 - this.data.newimgs.length, // 默认9
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片      
        var tempFilePaths = res.tempFilePaths;
        for (var x in tempFilePaths) {
          wx.showLoading({
            title: "加载中..."
          });

          _this.data.newimgs.push(tempFilePaths[x]);
          wx.uploadFile({
            url: 'https://www.dpsoft.top/upload.jsp', //仅为示例，非真实的接口地址
            filePath: tempFilePaths[x],
            name: 'file',
            formData: {
              'user': 'test'
            },
            success: function (res) {
              wx.hideLoading();

              var data = res.data
              console.log(data);
              data = JSON.parse(data);
              if ('ok' == data.status) {
                console.log(data.data);
                _this.data.imgWebPath.push(data.data);
              } else {
                wx.showToast({
                  title: data.msg,
                  icon: 'success',
                  duration: 2000
                });
              }


            },
            fail: function (res) {
              wx.hideLoading();
              wx.showToast({
                title: '失败',
                icon: 'success',
                duration: 2000
              });
            }
          });
        }

        _this.setData({
          newimgs: _this.data.newimgs
        });
      }
    })
  },
  onPublish: function () {

    var sql = "insert into star_dynamic (unionid,dynamic_txt,dynamic_img,location,lat,lng,createtime) values('" + app.globalData.unionid + "','" + encodeURI(this.data.newtxt) + "','" + this.data.imgWebPath + "','" + this.data.location.name + "'," + this.data.location.lat + "," + this.data.location.lng + ",now())";
    console.log(sql);
    wx.showLoading({
      title: "发表中..."
    });
    wx.request({
      url: 'https://www.dpsoft.top/db.jsp', //仅为示例，并非真实的接口地址
      method: 'POST',
      data: {
        sql: sql
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: res => {
        console.log(res.data);
        wx.hideLoading();

        /*
                wx.showTabBar({ animation: true });
                var animation = wx.createAnimation({ duration: 1000, timingFunction: 'ease' });
                animation.translateY(1000).step();
        
                this.setData({
                  animationData: animation.export(),
                  newtxt: '',
                  newimgs: [],
                  imgWebPath: [],
                  location: { 'name': '', 'lat': 0, 'lng': 0 }
                });
        */

        if ("ok" == res.data.status) {

          wx.navigateBack();

        } else {

        }

      }
    });
  },
  onNewtxt: function (event) {

    this.setData({
      newtxt: event.detail.value
    });
  },
  onCancel: function () {
    /*
    wx.showTabBar({ animation: true });
    var animation = wx.createAnimation({ duration: 1, timingFunction: 'step-start' });
    animation.translateY(1000).step();

    this.setData({
      animationData: animation.export(),
      newtxt: '',
      newimgs: [],
      imgWebPath: [],
      location: { 'name': '', 'lat': 0, 'lng': 0 }
    });


    wx.switchTab({ url: '/pages/index/index' });
    */
    wx.navigateBack();

  },
  chooseTargetAddr: function (e) {
    var _this = this;
    wx.chooseLocation({
      success: function (res) {

        console.log(res.name);
        _this.setData({

          location: {
            name: res.name, lat: res.latitude, lng: res.longitude
          }
        });
      },
      cancel: function () {

      },
      fail: function () {

      },
      complete: function () {

      }
    });
  }

};