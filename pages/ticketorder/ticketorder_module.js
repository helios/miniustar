var app = getApp();

module.exports = {

  /**
   * 页面的初始数据
   */
  data: {
    orderlist:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.loadData();


  },
  loadData : function(){
    var _this = this;

    app.getUnionid(function () {
      var sql = "select id,spot_name,spot_code,ticket_name,data_id,use_date,buy_count,amount,price,ordertime,contact_name,contact_tel,status,security_code,qrcode,unionid,seqno from ticket_order where unionid = '" + app.globalData.unionid + "' and status>0 order by ordertime desc";
      app.exesql(sql, function (res) {
        var orderlist = res.data.data;
        for (var x in orderlist) {
          orderlist[x].ordertime = _this.formatdate(new Date(orderlist[x].ordertime));
        }
        _this.setData({
          orderlist: orderlist
        });
      });
    });


  },
  onrefund: function(e){ 
    var _this = this;
    var id = e.currentTarget.dataset.id;
    wx.showModal({
      title: '退票确认',
      content: '您确定要退票吗？',
      success: function (res) {
        if (res.confirm) {
          console.log('用户点击确定')
          var sql = "update ticket_order set status = 3 where id="+id;
          app.exesql(sql,function(res){
            _this.loadData();
          });


        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  showdetail: function(e){
    var _this = this;
    var id = e.currentTarget.dataset.id;

    var url = "https://www.dpsoft.top/ticketorder.jsp?id=" + id;
    console.log(url);
    wx.navigateTo({
      url: '/pages/dbhtml/dbhtml?url=' + encodeURIComponent(url),
    })
  },
  formatdate: function (date) {

    var d = date.getDate();
    var m = date.getMonth() + 1;
    var y = date.getFullYear();
    if (m < 10) {
      m = '0' + m;
    };
    if (d < 10) {
      d = '0' + d;
    };
    return y + "-" + m + "-" + d;
  }
}