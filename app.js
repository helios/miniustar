//app.js
App({
  onLaunch: function () {
    //调用API从本地缓存中获取数据
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs);

var _this = this;
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
        wx.request({
          url: 'https://www.dpsoft.top/getopenid.jsp', //仅为示例，并非真实的接口地址
          data: {
            js_code: res.code
          },
          header: {
            'content-type': 'application/json' // 默认值
          },
          success: res => {
            this.globalData.openId = res.data.openid;
            this.globalData.session_key = res.data.session_key;
            this.globalData.unionid = res.data.unionid;

            wx.getUserInfo({
              success: function (res) {
                _this.globalData.userInfo = res.userInfo;

                var sql = "update user set nick_name='" + res.userInfo.nickName + "', head_img='" + res.userInfo.avatarUrl + "', gender=" + res.userInfo.gender + ",minopenid='" + _this.globalData.openId + "' where  unionid='" + _this.globalData.unionid + "'";
                _this.exesql(sql, function (res) {
                  if (parseInt(res.data.data) <= 0) {
                    var sql = "insert into user(uid,minopenid,name,nick_name, head_img, gender,unionid) values(REPLACE(UUID(),'-',''),'" + _this.globalData.openId + "','" + res.userInfo.nickName + "','" + res.userInfo.nickName + "','" + res.userInfo.avatarUrl + "'," + res.userInfo.gender + ",'" + _this.globalData.unionid + "')";
                    _this.exesql(sql);
                  }
                });

          }
        });

        
          }
        })
      }
    });

  },
  getUserInfo:function(cb){
    var that = this
    if(this.globalData.userInfo){
      typeof cb == "function" && cb(this.globalData.userInfo)
    }else{
      //调用登录接口
      wx.login({
        success: function () {
          wx.getUserInfo({
            success: function (res) {
              that.globalData.userInfo = res.userInfo
              typeof cb == "function" && cb(that.globalData.userInfo)
            }
          })
        }
      })
    }
  },
  getUnionid: function (cb) {
    var that = this
    if (this.globalData.unionid) {
      typeof cb == "function" && cb(this.globalData.unionid)
    } else {
      //调用登录接口
      wx.login({
        success: function (res) {
          // 发送 res.code 到后台换取 openId, sessionKey, unionId
          wx.request({
            url: 'https://www.dpsoft.top/getopenid.jsp', //仅为示例，并非真实的接口地址
            data: {
              js_code: res.code
            },
            header: {
              'content-type': 'application/json' // 默认值
            },
            success: res => {
              that.globalData.openId = res.data.openid;
              that.globalData.session_key = res.data.session_key;
              that.globalData.unionid = res.data.unionid;

              typeof cb == "function" && cb(that.globalData.unionid)
            }
          });
        }
      })
    }
  },
  globalData:{
    userInfo:null,
    openId:null,
    session_key:null,
    unionid:null,
    tabbar: {
      color: "#a9b7b7",
      selectedColor: "#0068C4",
      backgroundColor: "#FBFBFB",
      borderStyle: "white",
      list: [
        {
          pagePath: "/pages/index/index?page=circle",
          text: "大咖圈",
          iconPath: "/images/circle.png",
          selectedIconPath: "/images/circle_focus.png",
          selected: true
        },
        {
          "pagePath": "/pages/index/index?page=more",
          "text": "我的",
          "iconPath": "/images/me.png",
          "selectedIconPath": "/images/me_focus.png",
          selected: false
        }
      ],
      position: "bottom"
    }  
  },
  exesql:function(sql,sucFun,failFun,compFun){
    console.log(sql);

    wx.request({
      url: 'https://www.dpsoft.top/db.jsp', //仅为示例，并非真实的接口地址
      method: 'POST',
      data: {
        sql: sql
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: res => {
        console.log(res.data);

        if ("ok" == res.data.status) {
          typeof sucFun == "function" && sucFun(res);
        } else {
          typeof failFun == "function" && failFun(res);
        }
      },
      fail: function(e){
        typeof failFun == "function" && failFun(e);
      },
      complete: function (e) {
        typeof compFun == "function" && compFun(e);
      }
    });
  },
  editTabBar: function (tabIndex) {
    var tabbar = this.globalData.tabbar,
      currentPages = getCurrentPages(),
      _this = currentPages[currentPages.length - 1],
      pagePath = _this.__route__;
    (pagePath.indexOf('/') != 0) && (pagePath = '/' + pagePath);
   
    for (var i in tabbar.list) {
      tabbar.list[i].selected = false;
      
    }
    tabbar.list[tabIndex].selected = true;
    _this.setData({
      tabbar: tabbar
    });
  }
})